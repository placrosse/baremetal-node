# BareMetal Node

Insert summary here


## Prerequisites

These scripts depend on a Debian-based Linux system like [Ubuntu](https://https://www.ubuntu.com/download/desktop) or [Elementary](https://elementary.io/).

NASM (Assembly compiler) is required to build the loader and kernel, as well as the apps writen in Assembly. GCC (C compiler) is required for building the MCP, as well as the C applications. Git is used for pulling the software from GitLab.

In Ubuntu this can be completed with the following command:

	sudo apt-get install nasm gcc git


### Initial setup

	git clone https://gitlab.com/ReturnInfinity/BareMetal-Node.git
	cd BareMetal-Node
	./setup.sh


### Configuring a PXE boot environment

Notes on configuring a PXE book environment can be found [here](https://github.com/ReturnInfinity/BareMetal-Node/wiki/Configuring-a-PXE-boot-environment). Once this is complete run the following script to copy the binary to the tftpboot folder:

	sudo ./install.sh


## Using BareMetal Node


### Running the Master Control Program

The [MCP](http://tron.wikia.com/wiki/Master_Control_Program) (Master Control Program) is responsible for working with the nodes. Elevated access is required for sending/receiving Ethernet packets. Provide the interface name as an argument.

	sudo ./mcp INTERFACE

The MCP has several commands that can be run.

- discover
- dispatch
- execute
- exit


## Todo list

- Rewrite Node Handler in C instead of Assembly (eventually in will need to encrypt/decrypt packets)


// EOF
