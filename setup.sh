#!/bin/bash

mkdir -p sys
mkdir src
cd src
git clone https://gitlab.com/ReturnInfinity/Pure64.git
git clone https://gitlab.com/ReturnInfinity/BareMetal.git
cd ..
./build.sh
